<div align="center">
<h1>Shared Libraries: Remote Pipeline</h1>
    <a href="/docs/pre-commit.md">Pre-commit</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="/docs/organization.md">Organização do Repositorio</a>
</div>


## :raising_hand: O que é esse projeto?

Este projeto serve como Shared Libraries para a construção das pipelines no Gitlab.

## :construction: Pré-requisitos

Para utilização do projeto é necessário instalar o recurso pre-commit.
Para isso, basta executar o script setup.sh que está localizado na pasta
'scripts' no projeto.

## :walking: Como utilizar?

```yaml
include:
    - project: 'platform-enginnering/devops/shared-libraries/remote-pipeline'
      ref: 'main'
      file: 'platform-engineering/'$group'/'$aplication''
```

## :raised_hand: Considerações
```shell
Sensitive data are stored in a safe, for example, passwords and database access information, end others. Don´t commit these data to the repository.
```
## :raised_hands: Contribuidores

```bash
Marcos Cianci <marcos.cianci@gmail.com>
```

## :bookmark: Licensing
```bash
©2023 MarcosCianci – Todos os direitos reservados.
```
