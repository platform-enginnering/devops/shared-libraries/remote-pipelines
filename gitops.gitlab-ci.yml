include:
  - local: 'ci-pipeline/golang/setup.template.yml'
  - local: 'ci-pipeline/golang/test.template.yml'
  - local: 'ci-pipeline/common/build.template.yml'
  - local: 'ci-pipeline/common/sec.template.yml'
  - local: 'ci-pipeline/common/envs.template.yml'
  - local: 'ci-pipeline/common/sonar.template.yml'
  - local: 'cd-pipeline/deploy/argocd.template.yml'

cache:
  untracked: true
  key: $CI_COMMIT_REF_SLUG
  paths:
    - vendor/

stages:
  - setup
  - test
  - sec
  - envs
  - build
  - deploy

Setup:
  stage: setup
  image: sovchinn/builder
  extends: .base.setup
  artifacts:
    paths:
      - vendor/
  only:
    - merge_requests
    - develop
    - main

Test:
  extends: .base.test
  stage: test
  image: golang:latest
  only:
    - merge_requests
    - develop
    - main
  allow_failure: true

Code Quality:
  extends: .base.sonarqube
  stage: test
  image: sonarsource/sonar-scanner-cli:latest
  needs:
    - Test
  only:
    - merge_requests
    - develop
    - main
  allow_failure: true

InjectEnv:
  extends: .base.envs  
  stage: envs
  only:
    - merge_requests
    - develop
    - main

Trivy Scan:
  extends: .base.trivy.scan
  stage: sec
  allow_failure: true
  artifacts:
    reports:
      container_scanning: trivy-scan-report.json
  only:
    - merge_requests
    - develop
    - main

Trivy Image:
  extends: .base.trivy.image
  stage: sec
  image:
    name: docker:stable
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    IMAGE: trivy-ci-test:$CI_COMMIT_SHA
  allow_failure: true
  services:
    - name: docker:dind
      entrypoint: ["env", "-u", "DOCKER_HOST"]
      command: ["dockerd-entrypoint.sh"]
  artifacts:
    reports:
      container_scanning: trivyreport.json
  only:
    - merge_requests
    - develop
    - main

Build Dev:
  stage: build
  extends: .base.build.docker
  image:
    name: docker:latest
  services:
    - name: docker:19-dind
      alias: docker
  variables:
    VERSION_TAG: dev_$CI_COMMIT_SHORT_SHA
    DOCKER_DRIVE: overlay2
    CI_USER_REGISTRY: $CI_USER_REGISTRY
    CI_TOKEN_REGISTRY: $CI_TOKEN_REGISTRY
    CI_REGISTRY: $CI_REGISTRY
    extends: .base.build
  only:
    - merge_requests
    - develop

Build Prod:
  stage: build
  extends: .base.build.docker
  image:
    name: docker:latest
  services:
    - name: docker:19-dind
      alias: docker
  variables:
    VERSION_TAG: prod_$CI_COMMIT_SHORT_SHA
    DOCKER_DRIVE: overlay2
    CI_USER_REGISTRY: $CI_USER_REGISTRY
    CI_TOKEN_REGISTRY: $CI_TOKEN_REGISTRY
    CI_REGISTRY: $CI_REGISTRY
    extends: .base.build
  only:
    - merge_requests
    - main

Deploy Dev:
  extends: .base.deploy.argocd
  stage: deploy
  variables:
    GITLAB_USER: $GITLAB_USER
    GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
    CI_USER_REGISTRY: $CI_USER_REGISTRY
    GROUP: websites
    VERSION_TAG: dev_$CI_COMMIT_SHORT_SHA
  image: alpine
  only:
    - merge_requests
    - develop

Deploy Prod:
  extends: .base.deploy.argocd
  stage: deploy
  variables:
    GITLAB_USER: $GITLAB_USER
    GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
    CI_USER_REGISTRY: $CI_USER_REGISTRY
    GROUP: websites
    VERSION_TAG: prod_$CI_COMMIT_SHORT_SHA
  image: alpine
  only:
    - merge_requests
    - main
